<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cache_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
        date_default_timezone_set(ovoo_config('timezone'));
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}
	public function get_value_by_name($name)
	{
		if ($value = $this->cache->get($name)) {
			return $value;
		}
		return "";
	}
	public function save($name,$value,$time = 7200)
	{
		
		$this->cache->save($name,$value,$time);
	}
	public function delete_by_name($name)
	{
		$this->cache->delete($name);
	}

}