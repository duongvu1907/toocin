<style>
div#info-star {
    background: url(../assets/images/poster_background.jpg);
    padding: 50px;
    position: relative;
}

div#info-star:before {content: "";width: 100%;height: 100%;background: #00000099;top: 0px;position: absolute;left: 0px;}
.sign-avatar img {
    box-shadow: 1px 4px 17px #000000;
}
</style>
<div class="container">
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo base_url();?>"><i class="fi ion-ios-home"></i><?php echo trans('home'); ?></a>
        </li>
        <li class=""><?php echo trans('stars'); ?></li>
        <li class="active"><?php echo $star_name; ?></li>
    </ul>
</div>
<hr>    
<div id="info-star">
    <div class="container">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-4 ">
                <div class="sign-avatar text-center">
                    <img class="img-fluid" src="https://image.tmdb.org/t/p/w200<?php echo $star_thumbnail?>" alt="<?php echo $star_name ?>">
                </div>
                <div class="info text-center">
                    <h3><?php echo $star_name ?></h3>

                </div>
        </div>
        <div class="col-md-8">
            <ul id="info">
                <li>
                    <p><b>Known For</b>: <span><?php echo $details->known_for_department ?></span></p>
                </li>
                <li>
                    <p><b>Gender</b>: <span><?php echo $details->gender==1?"female":"male" ?></span></p>
                </li>
                <li>
                    <p><b>Date of Birth</b>: <span><?php echo $details->birthday ?></span></p>
                </li>
                <li>
                    <p><b>Place of Birth</b>: <span><?php echo $details->place_of_birth ?></span></p>
                </li>
                <?php if($details->deathday!=null): ?>
                <li>
                    <p><b>Death day</b>: <span><?php echo $details->deathday ?></span></p>
                </li>
                <?php endif ?>
                <li>
                    <p><b>Also Known As</b>: <span><?php echo implode(", ",$details->also_known_as) ?></span></p>
                </li>
                <li>
                    <p><b>Biography</b>: <span><?php echo $details->biography ?></span></p>
                </li>
                
            </ul>        
        </div>
        </div>
    </div>
</div>

<div id="section-opt">
    <div class="container">
        <div class="row">
            <h3>List Movies & TV Series</h3>
            <hr>
            <?php if($total_rows > 0):  ?>
                <div class="col-md-12 col-sm-12">
                    <div class="latest-movie movie-opt">
                        <div class="row clean-preset">
                            <div class="movie-container">
                                <?php foreach ($all_published_videos as $videos) :?>
                                    <div class="col-md-2 col-sm-3 col-xs-6">
                                        <?php include('thumbnail.php'); ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php if($total_rows > 24): echo $links; endif; ?>        
    </div>
</div>

